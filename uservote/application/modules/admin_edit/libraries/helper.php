<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Helper{

	public $constants;
	public function __construct()
	{
		$this->CI =& get_instance();
		$this->CI->load->config('registration/registration_config', TRUE);
	}

	public function showError($message, $errorMsg=null)
	{
		$status['code'] = ERROR_CODE;
		$status['message'] = $message;
		$status['errorMsg'] = $errorMsg;
		return json_encode($status);
	}

	public function showSuccess( $message, $data=NULL )
	{
		$status['code'] = SUCCESS_CODE;
		$status['message'] = $message;
		$status['data'] = $data;
		return json_encode($status);
	}

	public function _render_page($view, $data=null, $render=false)
	{
		$view_html = array( 
			$this->CI->load->view('base/header', $data, $render),
			$this->CI->load->view('registration/menu/header', $data, $render),
			$this->CI->load->view($view, $data, $render),  
			$this->CI->load->view('registration/menu/footer', $data, $render),
			$this->CI->load->view('base/footer', $data, $render)
			);
		if (!$render) return $view_html;
	}

	/*
        Input: message string, phone number array (all should be 10 digit numbers), json output is required or not
        Output:variable error with status true (message sent) or false (message not sent) in json format or array format depends on $json_response variable
    */

	public function send_many_sms($message = '', $phonenos = array(), $json_response = false)
	{
		$base_url = base_url();
		if(strpos($base_url, 'localhost')) return false;

		$error = array();
		$error['status'] = 'false';
		if(empty($phonenos)) $error['message'] = 'No phone number received. Please provide valid 10 digits phone number only.';
		if(empty($message)) $error['message'] = 'No message string received. Max 160 char';
		$phonenos = join(',',$phonenos);
		$url = "http://bulk.rocktwosms.com/spanelv2/api.php?username=".SMS_USERNAME."&password=".SMS_PASSWORD."&to=".$phonenos."&from=".SMS_FROM."&message=".urlencode($message);

		//Store data into URL variable
		// print_r($url);
		$ret =  file($url);    //Call Url variable by using file() function
		if(ctype_alnum($ret)){
			$error['status'] = 'true';
			$error['message'] = ' All SMS are successfully sent.';
		}
		else
			$error['message'] = 'Some error occured while sending SMS. Some or No SMS were sent.';
		if($json_response)
			return json_encode($error);
		else
			return $error; // return array
		echo $ret;    //$ret stores the msg-id
		// print_r($error);
	}

	public function send_sms( $message = NULL, $phoneno=NULL, $json_response = false )
	{
		$phonenos=array();
		if(!empty($phoneno))
			array_push($phonenos,$phoneno);
		return $this->send_many_sms( $message, $phonenos, $json_response );
	}
}

?>