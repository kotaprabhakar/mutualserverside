<!-- <div class="well hidden-print">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	<strong>Attendance module is under development.Mail your suggestions to WSDC (<a href="mailto:wsdc.nitw@gmail.com" target="_blank">wsdc.nitw@gmail.com</a>).</strong><a class="" href="javascript:void(0);" onclick="javascript:introJs().start();"> Click here for tutorial</a> or press Ctrl+ q 
</div>  -->
<script type="text/javascript">
	var session_id="<?php echo $session_id;?>";
</script>
<?php if($lists === FALSE): ?>
	<div class="alert alert-warning"> No faculty is alloted to any of the courses </div>
	<?php 
	$this->session->set_flashdata('warning', 'No data available for selected semester structure. Please try again.');
	redirect('registration/home');
	endif; 
	?>
	<div class="alert lead text-center hidden-print" id="startinfo">
		Select a course to continue
	</div>
	<div class="alert lead text-center hidden-print" id="startinfo2" style="display:none;">
		Select a date to see the list
	</div>
	<div class="form-inline">
		<div class="form-group">
			<label for="courses" class="control-label" >Courses: </label>
			<!-- <div class="col-md-8"> -->
			<select name="list" id="course-list" class="form-control input-sm" data-intro="Select course">
				<option value="">-- Select One --</option>
				<?php foreach($lists as $index => $list): ?>
					<option value="<?php echo $list->structure_id; ?>">
						<!-- <?php //echo $list->branch_name; ?> | -->
						<?php echo $list->class_name; ?>  |
						<?php echo $list->semester; ?> Semester |
						<?php echo $list->course_id ?> | 
						<?php echo $list->section ?>
					</option>
				<?php endforeach; ?>
			</select>
			<!-- </div> -->
			<input type="hidden" name="course_id" id="inputCourse_id" class="form-control" required="required">
			<input type="hidden" name="section" id="inputSection" class="form-control" required="required">
		</div>
		<div class='form-group'>
			<label for="date" class="control-label">Date:</label>
			<!-- <div class="col-md-8"> -->
			<input type='text' class="form-control input-sm" id='datetimepicker1' disabled="true" data-intro="Select date"/>
			<!-- </div> -->

		</div>
		<div class='form-group' id="display_hours" style="display:none;">
			<label for="date" class="control-label">No of hours:</label>
			<!-- <div class="col-md-8"> -->
			<input type='text' class="form-control input-sm num_hours" name="no_hours"/>
			<button class="btn btn-success btn-sm" data-id="0" data-intro="Update number of hours"><span class="glyphicon glyphicon-refresh"> Update</span></button>
			<!-- </div> -->
		</div>	
	</div>
	<br>

	<div id="course-attendance-data">
		<div class="row clearfix">
			<div class="col-md-12 column" id="loading-student-data" style="display:none;background:url(<?php echo asset_url();?>/images/loading.gif) no-repeat center; background-size:contain;height:50px;margin-top:25%;">
			</div>

			<div class="col-md-12 column" id="students-list" style="display:none;" >
				<p>
					<strong>Course Id :&nbsp;</strong><span id="course-id"></span> /
					<strong>Section :&nbsp;</strong><span id="section"></span> /
					<strong>Date :&nbsp;</strong><span id="date"></span> 
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<strong>Present:</strong> <span id="present"></span> /
					<strong>Absent:</strong> <span id="absent"></span>
				</p>
				<br>
					<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
							
					<legend>
					Working days date list
					</legend>
					
					</div>
						
					<div class="col-md-8">
					<legend>
					Students list
					</legend>
						<table class="table table-hover" id="table-student">
							<thead>
								<tr>
									<th>
										Roll
									</th>
									<th>
										Name
									</th>
									<th>
										Status
									</th>
								</tr>
							</thead>
							<tbody id="student-details">

							</tbody>
						</table>
					</div>
					<a href="#" class="scrollup" style="background:url(<?php echo asset_url();?>images/up.png) no-repeat;"></a>
				</div>
			</div>
		</div>