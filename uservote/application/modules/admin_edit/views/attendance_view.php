<div class="col-md-12">
	<div class="row hidden-print">
		<div class="alert alert-danger">
			<p>Attendance for Lab courses will start after correct lab batches allotment of students.If there is any other issue,please drop a mail to wsdc.nitw@gmail.com</p>
		</div>
	</div>
	<div class="row clearfix">
		<script type="text/javascript">
			var session_id="<?php echo $session_id;?>";
		</script>
		<?php if($lists === FALSE): ?>
			<div class="alert alert-warning"> No faculty is alloted to any of the courses </div>
			<?php 
			$this->session->set_flashdata('warning', 'No data available for selected semester structure. Please try again.');
			redirect('registration/home');
			endif; ?>
	<!-- <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12 column" id="startinfo">
		<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
			<legend>List view: Attendance record</legend>
			
		</div>
		<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
			<legend> Consolidated: Consolidated view of attendance</legend>
			
		</div>
	</div> -->

	<div class="col-md-12 col-sm-12 col-xs-12 col-lg-12" id="courses-list-div">
		<legend>Courses list</legend>
		<div class="list-group-item">
			<h4 class="list-group-item-heading">Course Id | Section</h4>
		</div>
		<div class="list-group courses-list clearfix">
			<?php $rowcount=1;
			foreach($lists as $index => $list): 
				if($list->type==1): 
					?>

				<div class="list-group-item course-item clearfix" 
				id="c<?php echo $rowcount;?>" data-id="<?php echo $rowcount;?>" data-structureid="<?php echo $list->structure_id; ?>" data-courseid="<?php echo $list->course_id ?>"
				data-section="<?php echo $list->section ?>" data-type="<?php echo $list->type ?>" data-lab-batch="<?php echo $list->lab_batch ?>">
				<div class="col-xs-7 col-sm-7 col-md-7 col-lg-7">
					<?php echo $list->branch_name; ?> | <?php echo $list->class_name; ?> | <?php echo $list->course_id ?>
					<?php if($list->section!="NO") echo '| '.$list->section.' Section'; ?>
					| <?php echo $list->name;?>
					<?php if($list->type==2) echo "| <strong> Batch ".$list->lab_batch."</strong>"; ?>
				</div>
				<div class="col-xs-5 col-sm-5 col-md-5 col-lg-5 text-right hidden-print">
					<a role="button" class="btn btn-info btn-sm" id="modal-322630" href="#modal-container-322630" data-toggle="modal" onclick="putCfid(<?=$list->cfid ?>)">Attendance Register</a>
					<button type="button" class="btn btn-danger btn-sm list"> <span class="glyphicon glyphicon-list-alt"></span> List</button>
					<button type="button" class="btn btn-info btn-sm consolidated"> <span class="glyphicon glyphicon-dashboard"></span> Consolidated</button>
				</div>
			</div>
			<?php $rowcount++;
			endif;
			endforeach; ?>
		</div>
	</div>
	<div class="modal fade" id="modal-container-322630" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					<h4 class="modal-title" id="myModalLabel" contenteditable="true">Attendance Register</h4>
				</div>
				<div class="modal-body clearfix" contenteditable="true">
					<div class="col-md-12 text-danger lead">
						Seven days register view from starting date
					</div>
					<div class="col-md-12 column">
							<div class="form-group">
								<label for="StartDate">Start Date</label>
								<input type="date" class="form-control" id="StartDate" name="StartDate" value="<?php echo date('Y-m-d') ?>" required/>
								<input type="hidden" class="form-control" id="cfid"/>
								
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-success" id="open_register">Submit</button>
								<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
							</div>
					</div>
				</div>

			</div>
		</div>
	</div>


	<div class="row clearfix hidden-print">
		<div class="col-md-12 column">
			<div class="row clearfix">
				<div class="col-md-3 column">

					<button type="button" class="btn btn-block btn-lg btn-info" id="go-back" style="display:none;"><span class="glyphicon glyphicon-chevron-left"></span> back to courses list</button>

				</div>
				<div class="col-md-9 column lead">
					<div class="col-md-9" id="course_name_div">
						<span id="course_name"></span>
						| <span id="course_id"></span>
						| <span id="sec"></span> Section
						<span id="lab_batch"></span>
					</div>
					<a role="button" class="btn btn-info pull-right" id="modal-322629" href="#modal-container-322629" data-toggle="modal">Add class</a>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="modal-container-322629" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					<h4 class="modal-title" id="myModalLabel" contenteditable="true">Add Class</h4>
				</div>
				<div class="modal-body clearfix" contenteditable="true">
					<div class="row form-group">
						<label for="date" class="col-sm-3 control-label">Select date: &nbsp;</label>
						<div class="col-sm-6 input-append">
							<input value="<?php echo date('Y-m-d') ?>" type='text' data-provide="datepicker" class="form-control input-sm" name="date" id='datetimepicker2' placeholder="Select date"/>
						</div>
						
					</div>

					<div class="row form-group">
						<label for="start" class="col-sm-3">Start time: &nbsp;</label>
						<div class="col-sm-9">
							<input class="form-control input-sm" id="start" value="8:00 AM"  required/>
						</div>
					</div>
					<div class="row form-group">
						<label for="end" class="col-md-3">End time:&nbsp;&nbsp;&nbsp;&nbsp;</label>
						<div class="col-sm-9">
							<input class="form-control input-sm" id="end" value="9:00 AM" required/>
						</div>
					</div>
					<script>
						$(document).ready(function() {
							function startChange() {
								var startTime = start.value();

								if (startTime) {
									startTime = new Date(startTime);

									end.max(startTime);

									startTime.setMinutes(startTime.getMinutes() + this.options.interval);

									end.min(startTime);
									end.value(startTime);
								}
							}

				                    //init start timepicker
				                    var start = $("#start").kendoTimePicker({
				                    	change: startChange,
				                    	interval:"60"
				                    }).data("kendoTimePicker");

				                    //init end timepicker
				                    var end = $("#end").kendoTimePicker().data("kendoTimePicker");

				                    //define min/max range
				                    start.min("8:00 AM");
				                    start.max("6:00 PM");

				                    //define min/max range
				                    end.min("8:00 AM");
				                    end.max("7:30 AM");
				                });
</script>
</div>
<div class="modal-footer">
	<button type="button" class="btn btn-sm btn-default" data-dismiss="modal" contenteditable="true">Close</button>
	<button type="button" id="add_date" class="btn btn-sm btn-success" data-dismiss="modal" contenteditable="true"><span class="glyphicon glyphicon-plus" > Add</button>
</div>
</div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
</div>
<div class="row" id="course-attendance-data" style="display:none;">
	<br>
	<div class="col-md-4 col-sm-4 col-xs-6 col-lg-4 column">
		<div class="col-md-12"><legend>Working days</legend></div>
		<div id="sticky-anchor"></div>
		<div class="list-group" id="dates-list">
		</div> 

	</div>
	<div class="col-md-8 col-sm-8 col-xs-6 col-lg-8 column hidden-print" id="start-date-details">
		<h3 class="text-center">Select a date or add a date</h3>
	</div>
	<div class="col-md-8 col-sm-12 col-xs-12 col-lg-8 column" id="loading-student-data" style="display:none;background:url(<?php echo asset_url();?>/images/updating.gif) no-repeat center; background-size:contain;height:50px;margin-top:25%;">

	</div>
	<div class="col-md-8 col-sm-12 col-xs-12 col-lg-8 column" id="students-list" style="display:none;" >
		<legend>Students list</legend>
		<div class="row">
			<table class="table-responsive table table-hover">
				<thead>
					<tr>
						<th>Date</th>
						<th>Start_time</th>
						<th>End Time</th>
						<th>No Hours</th>
						<th>Present</th>
						<th>Absent</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td id="date"></td>
						<td id="start_time"></td>
						<td id="end_time"></td>
						<td id="hours"></td>
						<td id="present"></td>
						<td id="absent"></td>

					</tr>
				</tbody>
			</table>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

			<button type="button" class="btn btn-success" id="update-attendance-record" data-loading-text="updating"><span class="glyphicon glyphicon-refresh"></span> Update attendance</button>
			<div class="col-md-12 col-sm-12 col-xs-12 col-lg-12 column lead" id="updating-student-data" style="display:none;background:url(<?php echo asset_url();?>/images/updating.gif) no-repeat center; background-size:contain;height:50px;margin-top:25%;">
			</div>

				<!-- <div class="alert alert-info">Click on the status icon to toggle the status.
					<p class="text-danger">Please dont use jquery table functions,its not updating for now.</p>
				</div> -->
				<br><br>	
				<table class="table table-hover table-condensed table-bordered" id="attendance-record-datewise">
					<thead>
						<tr>
							<th>
								Roll
							</th>
							<th>
								Name
							</th>
							<th>
								Status
							</th>
						</tr>
					</thead>
					<tbody id="student-details">

					</tbody>
				</table>
			</div>
		</div>
		<!-- </div> -->
	</div>
	<div class="col-md-12 col-sm-12 col-xs-12 col-lg-12 column" id="consolidated-view" style="display:none;">
		<br>
		<!-- <div class="col-md-12 column"> -->
		<legend>Consolidated report</legend>
	<!-- <ul class="nav nav-pills">
		<li class="active">
			<a href="javascript:void(0)"> <span class="badge pull-right" ></span>Total hours of classes held</a>
		</li>
	</ul> -->
	<br>
	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title">
				Course Info
			</h3>
		</div>
		<div class="panel-body">
			<ul class="list-inline list-unstyled" id="consolidated-report-courseinfo">
				<li>
					<strong>Course Id:</strong><span></span>
				</li>
				<li>
					<strong>Section:</strong><span></span>
				</li>
				<li>
					<strong>Total hours of classes held:</strong><span class="badge badge-important" id="no_classes_held"></span>
				</li>
			</ul>
		</div>

	</div>
	<table class="table table-hover" id="consolidated-report-table">
		<thead>
			<tr>
				<th>
					Roll No.
				</th>
				<th>
					Student Name
				</th>
				<th>
					[attended/total]
				</th>
				<th>
					Shortage ?
				</th>
			</tr>
		</thead>
		<tbody>

		</tbody>
	</table>
	<!-- </div> -->
</div>
<a href="#" class="scrollup" style="background:url(<?php echo asset_url();?>images/up.png) no-repeat;"></a>
</div>
<script>
$("#datetimepicker2").kendoDatePicker({
  animation: false,
  format: 'yyyy-MM-dd'
  });
</script>