<div class="notification-container city-add-alert hidden-print" style="display: none">
	<div class="notification-message">
		<button type="button" class="close btn-close-alert hidden-print" ><span aria-hidden="true">&times;</span></button>
			<span class='message'>
				<?=$status['message']?>
			</span>
	</div>
</div>
<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<div class="row form-group">
				<label for="inputCityName" id="CityName" class="col-sm-3 control-label">City Name <span class="text-danger">*</span> </label>
				<div class="col-sm-7" data-step="2" data-intro='Enter the course name here'>
					<input type="text" required="required" name="CityName" class="form-control input-sm" id="inputCityName" placeholder="Enter valid City Name">
				</div>
			</div>
			<div class="modal-footer" >
			<button type="button" class="btn btn-sm btn-success btn-add"> <span class="glyphicon glyphicon-refresh"></span> Add City </button>
			<button type="button" class="btn btn-sm btn-default" data-dismiss="modal"> <span class="glyphicon glyphicon-remove"></span> Close</button>
	</div>
	</div>
</div>
<script>
$('.btn-close-alert').click(function()
{
	$(this).closest('.notification-container').hide();
});

$('.btn-add').click(function(e){
	e.preventDefault();
	var cityName = $('#inputCityName').val();
	showAlert("LOADING");
	$.ajax({
		url: 'admin_edit/validateCity',
		type: 'post',
		data: {'cityName':cityName},
		beforeSend:function(){
		},
		success: function (data) {
			data = JSON.parse(data);
			console.log(data);
			if(data.code== 1) {
				var cityId = data.data;
				var content = "<option value='"+cityId+"'>"+cityName+"</option>";
				$("#inputCity").append(content);
				
			}
			showAlert(data.message);
		},
		error: function (jqXHR, textStatus, errorThrown)
		{
			showAlert("Some Error occured! Please reload/refresh the page and try again.");
			return false;
		}
	});
});

function showAlert( message )
{
	$('.city-add-alert .message').html( message );
	$('.city-add-alert').show();
}

function showLoadingAlert()
{
	$('.city-add-alert.message').html(LOADING);
	$('.city-add-alert').show();
}

function hideAlert()
{
	$('.city-add-alert').hide();
}
</script>
