<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Attendance Register</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">

	<!--link rel="stylesheet/less" href="less/bootstrap.less" type="text/css" /-->
	<!--link rel="stylesheet/less" href="less/responsive.less" type="text/css" /-->
	<!--script src="js/less-1.3.3.min.js"></script-->
	<!--append ‘#!watch’ to the browser URL, then refresh the page. -->
	
	<link href="<?php echo asset_url()."css/bootstrap.min.css" ?> " rel="stylesheet">
	<link href="<?php echo asset_url()."css/attendance_register.css" ?> " rel="stylesheet">
	

	<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
  	<!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <![endif]-->
    <script src="<?php echo asset_url()."js/jquery.js"; ?> "></script>
    <script src="<?php echo asset_url()."js/bootstrap.min.js"; ?> "></script>
</head>

<body>
	<div class="container-fluid">
		<div class="row clearfix">
			<div class="col-md-12 column">
				<div class="row clearfix">
					<div class="col-md-12 column">
						<h3 class="text-center">
							Attendance Register(Starting from </b><?php echo $start_date; ?>)
						</h3>
					</div>
				</div>
				<div class="row clearfix">
					<div class="col-md-12 column">
						<div class="panel panel-default">
							<div class="panel-heading">
								<strong>Course-Details</strong>
							</div>
							<div class="panel-body">
								<span id="course_name"><b>Course Name: </b><?php echo $course_faculty_details->course_name; ?></span>
								| <span id="course_id"><b>Course Id: </b><?php echo $course_faculty_details->course_id; ?></span>
								| <span id="sec"></span><b>Section: </b><?php if($course_faculty_details->section!=0) echo $course_faculty_details->section; else echo 'NO'; ?> Section
								<!-- |<span id="lab_batch"><b>Lab Batch: </b><?php echo $course_faculty_details->lab_batch; ?></span> -->
							</div>
							<!-- <div class="panel-footer">
								<strong>Faculty: </strong>
								<span id="course_name"><?php echo $course_faculty_details->faculty_name; ?></span>
							</div> -->
						</div>
					</div>
					<!-- <div class="col-md-6 column">
							<div class="panel panel-default">
								<div class="panel-heading">
									<strong>Statistics</strong>
								</div>
								<div class="panel-body">
									<div class="col-md-6">
										
									</div>
									<div class="col-md-6">
										
									</div>
								</div>
							</div>
					</div> -->
				</div>
		<br>
		<!-- <hr> -->
		<div class="alert alert-info hidden-print">
			Hover on the date header in table column to see full information about class date and timing.Dates are kept short for printing view.
			Eg : Hovering on 23-12 will show 23/12/2014 11:00:00 to 12:00:00
		</div>
		<div class="row clearfix">
			<div class="col-md-12 column">
				<table class="table table-bordered table-responsive">
					<thead>
						<tr class="table_header">
							<th class="" style="width:70px;">
								Roll No
							</th>
							<th class="" style="width:200px;">
								Name
							</th>
							<?php
							if(isset($dates) and sizeof($dates)>0){
								foreach ($dates as $key => $value):
									if($key==$no_days)
										break;
									$split=explode('-',$value['date']);
									$class_date=$split[2].'/'.$split[1].'/'.$split[0];  
									?>
									<th class="text-center" style="" title="<?php echo $class_date.' '.$value['start'].' to '.$value['end']; ?>" rowspan="3">
										<?php //echo $key+1; ?>
										<span><?php echo $split[2].'-'.$split[1]; ?></span>
									</th>

									<?php
									endforeach;
								}
								else
									$key=-1;
								for ($i=$key; $i < $no_days-1 ; $i++) 
								{
									echo '<th colspan="" rowspan="" headers="" scope=""></th>';
								} 
								?>
							</tr>
						</thead>
						<tbody>
							<?php foreach ($students_list as $key => $value): ?>
								<tr>
									<td class="" style="">
										<?php echo $value['roll']; ?>
									</td>
									<td class="" style="">
										<?php echo $value['name']; ?>
									</td>

									<?php
									if(isset($dates) and sizeof($dates)>0){
										foreach ($dates as $idx => $each_class): 
											if($idx==$no_days)
												break;
											?>
											<?php 
											if(array_search($value['roll'],$absents[$each_class['id']],true)===FALSE)
												$present=1;
											else
												$present=0;
											?>
											<td class="">
												<?php echo ($present==1) ? "<span class='present'>P</span>" : "<span class='absent'>A</span>"?>
											</td>

											<?php
											endforeach;
										}
										else
											$idx=-1;
										for ($i=$idx; $i < $no_days-1 ; $i++) 
										{
											echo '<td></td>';
										} 

										?>
									</tr>
								<?php endforeach; ?>
								<tr>
									<td colspan="" rowspan="" headers=""></td>
									<td colspan="" rowspan="" headers="">Absents</td>
									<?php
									if(isset($dates) and sizeof($dates)>0){
										foreach ($dates as $idx => $each_class): 
											// var_dump($each_class);
											if($idx==$no_days)
												break;
											?>
											
											<td class="">
												<?php echo sizeof($absents[$each_class['id']]); ?>
											</td>

											<?php
											endforeach;
										}
										else
											$idx=-1;
										for ($i=$idx; $i < $no_days-1 ; $i++) 
										{
											echo '<td></td>';
										}
										?>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
