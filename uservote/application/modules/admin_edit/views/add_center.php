<div class="notification-container center-add-alert hidden-print" style="display: none">
	<div class="notification-message">
		<button type="button" class="close btn-close-alert hidden-print" ><span aria-hidden="true">&times;</span></button>
		<span class='message'>
			<?=$status['message']?>
		</span>
	</div>
</div>
<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div class="row form-group">
			<label for="inputcenterId" class="col-sm-4 control-label">City </label>
			<div class="col-sm-7" data-step="3" data-intro='Select center'>
				<select name="cityId" id="inputCityId" class="form-control input-sm">
					<option value="">-- Select One --</option>
					<?php foreach($city AS $type): ?>
					<option value=<?=$type['cityId']?>><?=$type['cityName']?></option>
				<?php endforeach; ?>
			</select>
		</div>
	</div>
</div>
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
	<div class="row form-group">
		<label for="inputcenterName" id="centerName" class="col-sm-4 control-label">Center Name </label>
		<div class="col-sm-7" data-step="2" data-intro='Enter the center name here'>
			<input type="text" required="required" name="centerName" class="form-control input-sm" id="inputcenterName" placeholder="Enter valid center Name">
		</div>
	</div>
	<div class="modal-footer" >
		<button type="button" class="btn btn-sm btn-success btn-add"> <span class="glyphicon glyphicon-refresh"></span> Add center </button>
		<button type="button" class="btn btn-sm btn-default" data-dismiss="modal"> <span class="glyphicon glyphicon-remove"></span> Close</button>
	</div>
</div>
</div>
<script>
$('.btn-close-alert').click(function()
{
	$(this).closest('.notification-container').hide();
});

$('.btn-add').click(function(e){
	e.preventDefault();
	var centerName = $('#inputcenterName').val();
	var cityId = $('#inputCityId').val();
	showAlert("LOADING");
	$.ajax({
		url: 'admin_edit/validatecenter',
		type: 'post',
		data: {'centerName':centerName,'cityId':cityId},
		beforeSend:function(){
		},
		success: function (data) {
			data = JSON.parse(data);
			console.log(data);
			if(data.code== 1) {
				//$("#inputcenter").append(content);
				
			}
			showAlert(data.message);
		},
		error: function (jqXHR, textStatus, errorThrown)
		{
			showAlert("Some Error occured! Please reload/refresh the page and try again.");
			return false;
		}
	});
});

function showAlert( message )
{
	$('.center-add-alert .message').html( message );
	$('.center-add-alert').show();
}

function showLoadingAlert()
{
	$('.center-add-alert.message').html(LOADING);
	$('.center-add-alert').show();
}

function hideAlert()
{
	$('.center-add-alert').hide();
}
</script>
