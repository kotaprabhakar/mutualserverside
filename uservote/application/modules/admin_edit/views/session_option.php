<?php //print_r($branches) ?>

<form name="form_select_class" id="form-cfap"  method="POST" role="form" class="form form-horizontal" action="<?php echo base_url('registration/attendance/display') ?>">
	
	<div class="form-group">
		<label for="inputSession" class="control-label col-md-3">Session</label>
		<div class="col-md-9">
			<select name="session" id="inputSession" class="form-control input-sm" required="required">
				<?php foreach($sessions as $index => $session): ?>
					<option value="<?php echo $session->session_code ?>"><?php echo $session->session_name; ?></option>
				<?php endforeach; ?>
			</select>
		</div>
	</div>
	<div class="modal-footer">
		<button type="submit" id="btn_select_class"  data-loading-text="Searching"  class="btn btn-primary"> Generate courses</button>
		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	</div>
</form>
