<div class=" row hidden-print">
	<div class="notification-container city-alert" style="display: none">
		<div class="notification-message">
			<button type="button" class="close btn-close-alert hidden-print" ><span aria-hidden="true">&times;</span></button>
			<span class='message'></span>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
			<div class="row hidden-print">
				<div class="col-sm-12">
					<div class="btn-group pull-left">
						<button class="hidden-print btn btn-sm btn-primary helper_modal" data-toggle="modal" data-target="#helper_modal" data-heading="Add city" data-src="<?php echo base_url('admin_edit/addCity') ?>">
							<span class="glyphicon glyphicon-plus-sign"></span> Add City
						</button>
					</div>
				</div>
			</div>
			<br>
			<div class="row">
				<div class="form-group col-sm-8">
					<select data-step='5' data-intro='city Name' name="city" id="inputCity" class="form-control input-sm advance-select" required="required">
						<option value="">-- Select city --</option>
						<?php foreach($city as $city){ ?>
							<option value=<?php echo $city['cityId']; ?>>
								<?php echo $city['cityName'];?>
							</option>
						<?php } ?>
					</select>
				</div>
				<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 hidden-print">
					<button type="submit" data-step='9' data-intro='Next Step' class="btn btn-success btn-sm btn-next hidden-print"><span class="glyphicon glyphicon-refresh"></span></button>
				</div>
			</div>
		<br><br>
	</div>
	<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" style="display:none" id="center-list-div">
		<div class="row hidden-print">
			<div class="col-sm-12">
				<div class="btn-group pull-right">
					<button  class="hidden-print btn btn-sm btn-primary helper_modal" data-toggle="modal" data-target="#helper_modal" data-heading="Add Center" data-src="<?php echo base_url('admin_edit/addCenter') ?>">
						<span class="glyphicon glyphicon-plus-sign"></span> Add Center
					</button>
					<button data-step="1" data-intro="Click here to print" type="button" onclick="window.print()" class="hidden-print btn btn-sm btn-primary"><span class="glyphicon glyphicon-print"></span> Print</button>
				</div>
			</div>
		</div>
		<br>
		<table class="table table-condensed table-bordered table-hover">
			<thead>
			<th>#</th>
			<th>Center Name</th>
			<th data-step="2" data-intro='Click here to Edit the course' class="text-center hidden-print">Option</th>
			</thead>
			<tbody id="center-details">
			</tbody>
		</table>
	</div>
</div>

