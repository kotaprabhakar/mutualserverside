<div class="notification-container specialization-edit-alert hidden-print" style="display: none">
	<div class="notification-message">
		<button type="button" class="close btn-close-alert hidden-print" ><span aria-hidden="true">&times;</span></button>
			<span class='message'>
				<?=$status['message']?>
			</span>
	</div>
</div>
<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div class=" row form-group hidden ">
				<label for="inputSpecializationId" class="col-sm-3 control-label">Specialization Id &nbsp </label>
				<div class="col-sm-7">
					<input type="text" name="SpecializationId" class="hidden form-control input-sm" id="inputSpecializationId"  value="<?php echo isset($specialization[0]['specializationId'])? $specialization[0]['specializationId'] :'';?>">
					<p class="form-control-static">
					<?php echo isset($specialization[0]['specializationId']) ? $specialization[0]['specializationId'] : '' ?>
					</p>
				</div>
			</div>
			<div class="row form-group">
				<label for="inputSpecializationName" class="col-sm-4 control-label">Specialization Name <span class="text-danger">*</span> </label>
				<div class="col-sm-7" data-step="1" data-intro='Enter the Specialization name here'>
					<input type="text" required="required" name="SpecializationName" class="form-control input-sm" id="inputSpecializationName" placeholder="Valid Couse Name" value="<?php echo isset($specialization[0]['specializationName']) ? $specialization[0]['specializationName']  : ''?>">
				</div>
			</div>
			<div class="modal-footer" >
			<button type="button" class="btn btn-sm btn-success btn-edit"> <span class="glyphicon glyphicon-refresh"></span> Edit Specialization </button>
			<button type="button" class="btn btn-sm btn-default" data-dismiss="modal"> <span class="glyphicon glyphicon-remove"></span> Close</button>
	</div>
	</div>
</div>


<script>
$('.btn-close-alert').click(function()
{
	$(this).closest('.notification-container').hide();
});

$('.btn-edit').click(function(e){
	e.preventDefault();
	var specializationId = $('#inputSpecializationId').val();
	var specializationName = $('#inputSpecializationName').val();
	var button = rowSelected.find(':nth-child(3)').html();
	showAlert(LOADING);
	$.ajax({
		url: '../dept_spec/save',
		type: 'post',
		data: {'specializationId':specializationId,'specializationName':specializationName},
		beforeSend:function(){
			
		},
		success: function (data) {
			data = JSON.parse(data);
			console.log(data);
			if(data.code==1) {
				rowSelected.find(':nth-child(2)').html(specializationName);
				rowSelected.find(':nth-child(3)').html(button);
			}
			showAlert(data.message);
		},
		error: function (jqXHR, textStatus, errorThrown)
		{
			showAlert("Some Error occured! Please reload/refresh the page and try again.");
			return false;
		}
	});
});

function showAlert( message )
{
	$('.specialization-edit-alert .message').html( message );
	$('.specialization-edit-alert').show();
}

function showLoadingAlert()
{
	$('.specialization-edit-alert .message').html(LOADING);
	$('.specialization-edit-alert').show();
}

function hideAlert()
{
	$('.specialization-edit-alert').hide();
}
</script>
