<!-- <div class="well hidden-print">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	<strong>Attendance module is under development.Mail your suggestions to WSDC (<a href="mailto:wsdc.nitw@gmail.com" target="_blank">wsdc.nitw@gmail.com</a>).</strong><a class="" href="javascript:void(0);" onclick="javascript:introJs().start();"> Click here for tutorial</a> or press Ctrl+ q 
</div>  -->
<script type="text/javascript">
	var session_id="<?php echo $session_id;?>";
</script>
<?php if($lists === FALSE): ?>
	<div class="alert alert-warning"> No faculty is alloted to any of the courses </div>
	<?php 
	$this->session->set_flashdata('warning', 'No data available for selected semester structure. Please try again.');
	redirect('registration/home');
	endif; ?>

	<legend>CSE / B.Tech / Semester 1 /CS390 / A section</legend>
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
			<form class="form-horizontal" method="post" action="<?php echo base_url('profile/detail/do_adding'); ?>" role="form" onsubmit="return false;" data-step="1" data-intro="Add course details here">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<legend><small>Course details</small></legend>
						<div class="form-group">
							<label for="courses" class="control-label col-md-3">Courses: </label>
							<div class="col-md-8">
								<select name="list" id="course-list" class="form-control input-sm ">
									<option value="">-- Select One --</option>
									<?php foreach($lists as $index => $list): ?>
										<option value="<?php echo $list->structure_id; ?>">
											<!-- <?php //echo $list->branch_name; ?> | -->
											<?php echo $list->class_name; ?>  |
											<?php echo $list->semester; ?> Semester |
											<?php echo $list->course_id ?> | 
											<?php echo $list->section ?>
										</option>
									<?php endforeach; ?>
								</select>
							</div>
							<input type="hidden" name="course_id" id="inputCourse_id" class="form-control" required="required">
							<input type="hidden" name="section" id="inputSection" class="form-control" required="required">
						</div>
						<div class='form-group'>
							<label for="date" class="control-label col-md-3">Date</label>
							<div class="col-md-8">
								<input type='text' class="form-control input-sm" id='datetimepicker1' disabled="true"/>
							</div>
						</div>
						<div class='form-group'>
							<label for="date" class="control-label col-md-3">Type</label>
							<div class="col-md-8">
								<div class="radio">
									<label>
										<input type="radio" name="ramu" id="input" value="">
										Present
									</label>
								</div>
								<div class="radio">
									<label>
										<input type="radio" name="ramu" id="input" value="" checked="checked">
										Absent
									</label>
								</div>
							</div>
						</div>
						<div class='form-group' >
							<label for="inputRollNo" class="control-label col-md-3">Roll No.</label>
							<div class="col-md-8">
								<input type="text" name="rollNo" id="inputRollNo" class="form-control input-sm" value="" required="required" title="">
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
			<button type="button" class="btn btn-success btn-block "><span class="glyphicon glyphicon-refresh"></span> Update Attendacne</button>
			<br>
			<ul class="list-group">
				<li class="list-group-item">114105 | Vaibhav Awachat <span class="label label-danger pull-right tips" title="Click to mark present">marked absent</span></li>
				<li class="list-group-item">114105 | Vaibhav Awachat <span class="label label-danger pull-right tips" title="Click to mark present">marked absent</span></li>
				<li class="list-group-item">114105 | Vaibhav Awachat <span class="label label-danger pull-right tips" title="Click to mark present">marked absent</span></li>
				<li class="list-group-item">114132 | Abhishek Singh <span class="label label-success pull-right">marked present</span></li>
			</ul>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
			
		</div>
	</div>