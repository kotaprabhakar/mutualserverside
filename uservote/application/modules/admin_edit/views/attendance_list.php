<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<legend>Registered Students List</legend>
		<div class="panel panel-default">
			<div class="panel-body">
				<div class="row">
					<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
						<table>
							<tbody>
								<tr>
									<td><label>Branch</label></td>
									<td>:</td>
									<td> <?php echo $branch;?></td>
								</tr>
								<tr>
									<td><label>class</label></td>
									<td>:</td>
									<td><?php echo $class; ?></td>
								</tr>
								<tr>
									<?php if(!empty($subject)): ?>
										<td><label>Subject</label></td>
										<td>:</td>
										<td><?php echo $subject[0]->name.' ('.$subject[0]->cid.')' ?></td>
									<?php else: ?>
										<td></td>
										<td></td>
										<td></td>
									<?php endif; ?>
								</tr>

							</tbody>
						</table>
					</div>
					<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
						<table>
							<tbody>
								<tr>
									<td><label>Semester</label></td>
									<td>:</td>
									<td><?php echo $semester; ?>
									</td>
								</tr>
								<tr>
									<td><label>Section</label></td>
									<td>:</td>
									<td><?php echo $section; ?>
									</td>
								</tr>
								<tr>
									<td><label>Date</label></td>
									<td>:</td>
									<td> <?php echo date('d-m-y'); ?></td>
								</tr>

							</tbody>
						</table>
					</div>
				</div>
				
			</div>
			
		</div>
		
		<table class="table table-hover table-condensed table-bordered">
			<thead>
				<tr>
					<th>#</th>
					<th>Roll No.</th>
					<th>Student Name</th>
					<th>Signature</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach($lists as $index => $item):?>

					<tr>
						<td><?php echo $index+1 ?></td>
						<td><?php echo $item['roll']?></td>
						<td><?php echo $item['name'] ?></td>
						<td></td>
					</tr>

				<?php endforeach; ?>
			</tbody>
		</table>

	</div>
</div>
