    <div class="row">
      <div class="col-xs-12 col-sm-3 col-md-2 hidden-print" id="sidebar" role="navigation">
        <div class="bs-sidebar hidden-print affix" role="complementary">
         <ul class="nav bs-sidenav">
          <li <?php echo $current_page === "home" ? "class='active'" : ""?>><a href="<?php echo base_url('registration/home'); ?>"><span class="glyphicon glyphicon-home"></span> Home</a></li>
          <?php if(isset($is_hod) and $is_hod === TRUE): ?>
          <!-- <li>
          <a href="<?php echo base_url('registration/hod'); ?>"><span class="glyphicon glyphicon-list-alt"></span> Faculty Advisor (FAAP)</a></li> -->
        <?php endif; ?>
        <li <?php echo $current_page === "faap" ? "class='active'" : ""?>>
          <a href="<?php echo base_url('registration/faap') ?>"><span class="glyphicon glyphicon-user"></span> Allot faculty advisor</a>
        </li>

        <li <?php echo $current_page === "allot" ? "class='active'" : ""?>>
          <a href="#" class="helper_modal" data-toggle="modal" data-target="#helper_modal"  data-heading="Facullty allotment to courses" data-src="<?php echo base_url('registration/cfap') ?>"><span class="glyphicon glyphicon-compressed"></span> Allot faculty to courses</a>
        </li>

        <li <?php echo $current_page === "alloted" ? "class='active'" : ""?>>
          <a href="#" class="helper_modal" data-toggle="modal" data-target="#helper_modal"  data-heading="Generate allotment status report" data-src="<?php echo base_url('registration/cfap/cfap_list_options') ?>"><span class="glyphicon glyphicon-folder-open"></span> Allotment status report</a>
        </li>
        <!-- <li>
        <a href="<?php echo base_url('registration/structure') ?>"><span class="glyphicon glyphicon-th-list"></span> Semester Structure</a></li>  -->
        <hr>
        <!--  <li ><a href="#" class="helper_modal" data-toggle="modal" data-target="#helper_modal"  data-heading="Register new student" data-src="<?php echo base_url('registration/newcandidate') ?>"><span class="glyphicon glyphicon-user"></span> Register new candidate</a></li> -->
        <li <?php echo $current_page === "register_new" ? "class='active'" : ""?>>
          <a href="<?php echo base_url('registration/newcandidate'); ?>"><span class="glyphicon glyphicon-user"></span> Register new candidate</a>
        </li>

        <li <?php echo $current_page === "slip" ? "class='active'" : ""?>>
          <a href="<?php echo base_url('registration/slip'); ?>"><span class="glyphicon glyphicon-book"></span> Registration Slip</a>
        </li>

        <li <?php echo $current_page === "registered" ? "class='active'" : ""?>>
          <a href="#" class="helper_modal" data-toggle="modal" data-target="#helper_modal"  data-heading="Generate list of Registered Students" data-src="<?php echo base_url('registration/a') ?>"><span class="glyphicon glyphicon-edit"></span> Registered students list</a>
        </li>

        <li <?php echo $current_page === "status" ? "class='active'" : ""?>>
          <a href="<?php echo base_url('registration/status') ?>"><span class="glyphicon glyphicon-ok"></span> Registration status</a>
        </li>

        <li <?php echo $current_page === "attendance" ? "class='active'" : ""?>>
          <a href="#" class="helper_modal" data-toggle="modal" data-target="#helper_modal"  data-heading="Generate attendance sheet of registered students" data-src="<?php echo base_url('registration/attendance/options') ?>"><span class="glyphicon glyphicon-th-list"></span> Attendance Sheet</a>
        </li>

        <hr>
        <!-- <li>
        <a href="#" class="bold">Courses</a></li> -->
        <!-- <ul class="nav"> -->
        <li <?php echo $current_page === "course_add" ? "class='active'" : ""?>>
          <a href="<?php echo base_url('registration/courses/add') ?>"><span class="glyphicon glyphicon-plus-sign"></span> Add new course</a>
        </li>
        <li <?php echo $current_page === "course_edit" ? "class='active'" : ""?>>

          <a href="<?php echo base_url('registration/courses/edit') ?>"><span class="glyphicon glyphicon-pencil"></span> Edit existing course</a>
        </li>

        <li <?php echo $current_page === "courses_list" ? "class='active'" : ""?>>
          <a href="<?php echo base_url('registration/courses/listall') ?>"><span class="glyphicon glyphicon-list-alt"></span> List all courses</a>
        </li>

        <!-- </ul> -->
        <!-- <li>
        <a href="#">Option 2</a></li> -->
      </ul>
    </div><!--/span-->
  </div>
  <div class="clearfix visible-xs hidden-print"></div>
  <div class="col-xs-12 col-sm-9 col-md-10">
    <br>