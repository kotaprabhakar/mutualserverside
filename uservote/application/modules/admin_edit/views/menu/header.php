<div class="row">
  <div class="col-xs-12 col-sm-3 col-md-2 hidden-print" id="sidebar" role="navigation">
    <div class="bs-sidebar hidden-print affix" role="complementary">
     <ul class="nav nav-list bs-sidenav">
      <li <?php echo $current_page === "city_center" ? "class='active'" : ""?>>
        <a href="<?php echo base_url('admin_edit'); ?>">
          <span class="glyphicon glyphicon-home"></span>City Center
        </a>
      </li>
        <li <?php echo $current_page === "Users" ? "class='active'" : ""?>>
      <a href="<?php echo base_url('auth') ?>">
           <span class="glyphicon glyphicon-th-list"></span> Users
      </a>
    </li>
    </ul>
</div>
</div>
<div class="clearfix visible-xs hidden-print"></div>
<div class=" removePaddingLeft col-xs-12 col-sm-9 col-md-10">
  <br>