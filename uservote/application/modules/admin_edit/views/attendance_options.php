<?php //print_r($branches) ?>

<form name="form_select_class" id="form-cfap"  method="POST" role="form" class="form form-horizontal" action="<?php echo base_url('registration/attendance/listall') ?>">
	<?php if($lists === FALSE): ?>
		<div class="alert alert-warning"> No faculty is alloted to any of the courses </div>
	<?php endif; ?>
	<div class="form-group">
		<label for="inputSession" class="control-label col-md-3">Session</label>
		<div class="col-md-9">
			<select name="session" id="inputSession" class="form-control input-sm" required="required">
				<?php foreach($sessions as $index => $session): ?>
					<option value="<?php echo $session->session_code ?>"><?php echo $session->session_name; ?></option>
				<?php endforeach; ?>
			</select>
		</div>
	</div>
	<div class="form-group">
		<label for="inputList" class="control-label col-md-3">Select Class</label>
		<div class="col-md-9">
			<select name="list" id="inputList" class="form-control input-sm ">
				<option value="">-- Select One --</option>
				<?php foreach($lists as $index => $list): ?>
					<option value="<?php echo $list->structure_id; ?>">
						<?php echo $list->branch_name; ?> |
						<?php echo $list->class_name; ?>  |
						<?php echo $list->semester; ?> Semester |
						<?php echo $list->course_id ?> | 
						<?php echo $list->section ?>
						<?php if($list->type==1) echo "| <strong> Batch ".$list->lab_batch."</strong>"; ?>
					</option>
				<?php endforeach; ?>
				<!-- <option value="">-- Other --</option> -->
			</select>
		</div>
	</div>
	<input type="hidden" name="course_id" id="inputCourse_id" class="form-control" required="required">
	<input type="hidden" name="section" id="inputSection" class="form-control" required="required">
	<input type="hidden" name="lab_batch" id="inputLab_batch" class="form-control" required="required">

	<div class="modal-footer">
		<button type="submit" id="btn_select_class"  data-loading-text="Searching"  class="btn btn-primary"> Generate sheet</button>
		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	</div>
</form>

<script>

	$('#inputList').change(function (e) {
		e.preventDefault();
		var structure = $("#inputList :selected").text();
		structure = structure.split("|");
		$('#inputCourse_id').val($.trim(structure[3]));
		$('#inputSection').val($.trim(structure[4]));
		if(structure[5]===undefined)
			return;
		batch=structure[5].split(" ");
		$("#inputLab_batch").val($.trim(batch[3]));
		//console.log($.trim(batch[3]));
	});

	$(function() {
		$('#inputList').change();
	});

</script>
