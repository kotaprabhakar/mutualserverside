<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//title
$lang['home_title'] = "Even Semester Registration | Home";

//General Errors 
$lang['ErrorInvalidParameter'] = "Invalid parameter entered. Please reload/refresh the page and try again.";
$lang['ErrorPermissionDenied'] = "You do not have permission to access this feature.";
$lang['ErrorDataFetching']='Error in fetching Data. Please reload/refresh the page and try again.';
$lang['SuccessDataRetrieval']='Data fetched and updated successfully';

//Session
$lang['ErrorSessionDetails'] = 'Error loading Session Details. Please refresh/reload and try again.';

//Semester
$lang['ErrorSemesterDetails'] = 'Error loading Semesters. Please refresh/reload and try again.';
$lang['SuccessSemesterDetails'] = 'Semesters loaded successfully.';

//City
$lang['ErrorCityDetails'] = 'Error loading City. Please refresh/reload and try again.';
$lang['SuccessCityDetails'] = 'Cities loaded successfully.';
$lang['SuccessAddedCity'] = 'City added successfully';

//Center
$lang['ErrorCenterDetails'] = 'Error loading Centers. Please refresh/reload and try again.';
$lang['SuccessCenterDetails'] = 'Centers loaded successfully.';

//Course Allotment
$lang['ErrorCourseAlreadyAllotted'] = "Course is already allotted to the structure.";
$lang['ErrorDeleteCourseAllotted'] = 'Error in deleting course allotted. Please reload/refresh the page and try again.';
$lang['SuccessCourseAllotted'] = "Course is allotted successfully to the structure.";
$lang['SuccessDeletedCourseDetails'] = 'Course removed successfully.';

//Course Details
$lang['ErrorCourseDetailsMissing'] = 'CourseId entered is missing from Database. Please add the missing course first.';
$lang['SuccessCourseDetailsEdit'] = 'Course Successfullly Edited.';
$lang['SuccessAddedCourse'] = 'Course Successfullly added.';
$lang['ErrorCourseExists'] = 'Course already present. Please go to edit course.';
$lang['SuccessCourseDetailsDelete'] = 'Course Successfullly Deleted.';


//Course Mode
$lang['ErrorUpdateCourseMode'] = 'Error in updating Course Mode. Please reload/refresh the page and try again.';
$lang['SuccessUpdateCourseMode'] = "Course Mode updated successfully."; 

//Faculty Advisor Allotment
$lang['SuccessUpdateFacultyAdvisor'] = 'Faculty Advisor updated Successfully.';
$lang['ErrorUpdateFacultyAdvisor'] = 'Error in updating allocating faculty advisor. Please reload/refresh the page and try again.';
$lang['SuccessDeleteFacultyAdvisor'] = 'Faculty Advisor deleted Successfully';
$lang['ErrorDeleteFacultyAdvisor'] = 'Error in deleting allocating faculty advisor. Please reload/refresh the page and try again.';


//Faculty Allotment
$lang['ErrorUpdateFacultyAllotted'] = 'Error in updating faculty allotted. Please reload/refresh the page and try again.';
$lang['SuccessUpdateFacultyAllotted'] = "Faculty allotted successfully."; 

//Lab Batches
$lang['ErrorCreateNewBatch'] = 'Error in allotting new Batch. Please refresh/reload and try again.';
$lang['ErrorDeleteBatchAllotted'] = 'Error in deleting batch allotted. Please reload/refresh the page and try again.';
$lang['SuccessCreateNewBatch'] = 'New Batch allotted successfully.';
$lang['SuccessDeletedBatch'] = 'Batch removed successfully.';

//Section
$lang['ErrorDeleteSection'] = 'Error in removing section. Please reload/refresh the page and try again.';
$lang['SuccessDeleteSection'] = 'Section removed successfully';
$lang['SuccessAddSection'] = 'Section added successfully';

//Structure
$lang['ErrorInvalidStructure'] = 'Invalid Structure. Please reload/refresh the page and try again.';
$lang['ErrorStructureExists'] = 'Structure doses not exists. Allot faculty advisor for the structure first.';

//Registration
$lang['ErrorFetchingAllottedCourses'] = 'No courses are allotted to this Strcture. Please contact Admin.';
$lang['SuccessFetchingAllottedCourses'] =  'Allotted Courses fetched successfully.';
$lang['ErrorCourseNotAllotted'] = 'Course Id entered is not allotted to any structure.';
$lang['SuccessAddingBacklogCourse'] = 'Backlog course added successfully.';
$lang['ErrorRegistration'] = 'Error in registering student. Please reload/refresh the page and try again.';
$lang['SuccessRegistration'] = 'Student Registered successfully';
$lang['ErrorInvalidRegistrationNumber'] = 'Invalid Registration Number. Student must have a Student Portal account and his/her profile should be completed before registration.';
$lang['ErrorStudentRoll'] = 'Student Roll is missing from Student Data. Student needs to complete his Student Profile.';
$lang['ErrorStudentName'] = 'Student Name is missing from Student Data. Student needs to complete his Student Profile.';

//Registered
$lang['ErrorStudentAlreadyRegistered'] = 'Students is already Registered in this structure.';
$lang['ErrorNoStudentRegistered'] = 'No student registered in this course.';
$lang['ErrorStudentNotRegistered'] = 'Student is not registered.';
$lang['ErrorDeleteStudentRegistration'] = 'Error deleting Student Registration. Please reload/refresh the page and try again.';
$lang['SuccessDeleteStudentRegistration'] = 'Student Registration deleted successfully.';

//Roll List
$lang['SuccessBatchesFetched'] = 'Data Successfully fetched click on next';
$lang['ErrorPermissiondDeniedBatch'] = 'Batch Not Alloted To You';

//City Center
$lang['SuccessCenterDelete'] = 'Center successfully Deleted';
$lang['SuccessCenterEdit'] = 'Center successfully Edited';
$lang['SuccessAddedCenter'] = 'Center successfully Added';
