<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_edit extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//$this->load->library('auth/ion_auth');
		//if (!$this->ion_auth->logged_in())
		//	redirect('auth/login');
		$this->load->model('admin_edit_model');
		//$this->load->library(array('form_validation','registration/helper'));
		//$this->load->helper('language');
	}

	public function login()
	{
		$json = file_get_contents('php://input');
		$data = json_decode($json,TRUE);
		if(!empty($data['EmailID']))
		{
			$email = trim($data['EmailID']);
			$password = trim($data['Password']);
			$this->load->model('admin_edit_model_sec');
			$var = $this->admin_edit_model_sec->loginCheck($email,$password);
		}
		else
		{
			$var = 2;
		}
		echo json_encode(array('var'=>$var));
	}

	public function insertEducation()
	{
		$json = file_get_contents('php://input');
		$data = json_decode($json,TRUE);
		if(!empty($data['UserID']))
		{
		$i=1;
		foreach ($data['Education'] as $key) 
		{
		$new['field'] = $key['Field'];
		$new['institution'] = $key['Institution'];
		$new['user_id'] = $data['UserID'];
		$x = $this->admin_edit_model->insertEducationDetails($new);
		$i++;
		}
			if($x==true)
			{
			$var = 1;
			}
			else
			{
				$var = 0;
			}
		}
		else
		{
			$var = 2;
		}
		echo json_encode(array('var'=>$var));
	}

	public function insertUserData()
	{
		$json = file_get_contents('php://input');
		$obj = json_decode($json);
		if(($obj->Details->firstName!= NULL)&&($obj->Details->lastName!= NULL)&&($obj->Details->EmailID!= NULL)&&($obj->Details->Password!= NULL)&&($obj->Details->PhoneNo!= NULL))
		{
		$new['email'] = trim($obj->Details->EmailID);
		$new['password'] = trim($obj->Details->Password);
		$new['phone'] = trim($obj->Details->PhoneNo);
		$new['first_name'] = trim($obj->Details->firstName);
		$new['last_name'] = trim($obj->Details->lastName);
		$x = $this->admin_edit_model->insertGeneralDetails($new);
			if($x!=false)
			{
			$var = $x;
			}
			else
			{
				$var = 0;
			}
		}
		else
		{
			$var =  2;
		}
    	echo json_encode(array('var'=>$var)); 
	}

	public function insertPlaces()
	{
		$json = file_get_contents('php://input');
		$data = json_decode($json,TRUE);
		if(!empty($data['Places']['UserID']))
		{
		$new['home'] = $data['Places']['Home'];
		$new['place1'] = $data['Places']['Visited'][0];
		$new['place2'] = $data['Places']['Visited'][1];
		$new['place3'] = "";
		$new['user_id'] = $data['Places']['UserID'];
		$x = $this->admin_edit_model->insertPlacesDetails($new);
			if($x==true)
			{
			$var = 1;
			}
			else
			{
				$var = 0;
			}
		}
		else
		{
			$var =2;
		}
		echo json_encode(array('var'=>$var)); 
	}

	public function insertHobbies()
	{
		$json = file_get_contents('php://input');
		$data = json_decode($json,TRUE);
		if(!empty($data['UserID']))
		{
		foreach ($data['Hobbies'] as $key) 
		{
		$new['interest'] = $key;
		$new['user_id'] = $data['UserID'];
		$x = $this->admin_edit_model->insertInterestDetails($new);
		}
			if($x==true)
			{
			$var = 1;
			}
			else
			{
				$var = 0;
			}
		}
		else
		{
			$var = 2;
		}
		echo json_encode(array('var'=>$var)); 
	}

	public function insertCarriers()
	{
		$json = file_get_contents('php://input');
		$data = json_decode($json,TRUE);
		if(!empty($data['UserID']))
		{
		foreach ($data['Career'] as $key) 
		{
		$new['name'] = $key['Name'];
		$new['designation'] = $key['Designation'];
		$new['domain'] = $key['Domain'];
		$new['user_id'] = $data['UserID'];
		$x = $this->admin_edit_model->insertCarrerDetails($new);
		}
			if($x==true)
			{
			$var = 1;
			}
			else
			{
				$var = 0;
			}
		}
		else
		{
			$var = 2;
		}
		echo json_encode(array('var'=>$var)); 
	}


	public function getNotInList()
	{
		$json = file_get_contents('php://input');
		$data = json_decode($json,TRUE);
		if(!empty($data['UserID']))
		{

		}
		else
		{
			$var = 2;
		}

		echo json_encode(array('var'=>$var));
	}

	public function insertTask()
	{
		$json = file_get_contents('php://input');
		$data = json_decode($json,TRUE);
		if(!empty($data['UserID']))
		{
			$x = $this->admin_edit_model->insertTaskDetails($data['TaskDetails'],$data['UserID']);
			if($x != FALSE)
			{
			$var = $this->admin_edit_model->insertAssignes($data['AssignedPersons'],$x,0);
			$var = $x;
				// if(!empty($data['Keywords']))
				// {
				// $this->admin_edit_model->insertKeywords($data['Keywords'],$x);
				// }
			}
			else
			{
				$var = 2;
			}
		}
		else
		{
			$var = 2;
		}
		echo json_encode(array('var'=>$var));
	}
    
    public function assignMoreUsers()
    {
    	$json = file_get_contents('php://input');
		$data = json_decode($json,TRUE);
		if(!empty($data['UserID']))
		{
			if(!empty($data['TaskID']))
			{
			$var = $this->admin_edit_model->insertAssignes($data['AssignedPersons'],$data['TaskID'],$data['UserID']);
				// if(!empty($data['Keywords']))
				// {
				// $this->admin_edit_model->insertKeywords($data['Keywords'],$x);
				// }
			$var = $data['TaskID'];
			}
			else
			{
				$var = 2;
			}
		}
		else
		{
			$var = 2;
		}
		echo json_encode(array('var'=>$var));
    }

	public function getAssignedTask()
	{
		$json = file_get_contents('php://input');
		$data = json_decode($json,TRUE);
		if(!empty($data['UserID']))
		{
			$var = $this->admin_edit_model->getAllAssignedTask($data['UserID']);
		}
		else
		{
			$var = 2;
		}
		echo json_encode(array('var'=>$var));
	}


	public function getTask()
	{
		$json = file_get_contents('php://input');
		$data = json_decode($json,TRUE);
		if(!empty($data['UserID']))
		{
			$var = $this->admin_edit_model->getTaskDetails($data['UserID']);
		}
		else
		{
			$var = 2;
		}
		echo json_encode(array('var'=>$var));
	}


	public function getSharedTask()
	{
		$json = file_get_contents('php://input');
		$data = json_decode($json,TRUE);
		if(!empty($data['UserID']))
		{
			$var = $this->admin_edit_model->getSharedTaskDetails($data['UserID']);
		}
		else
		{
			$var = 2;
		}
		echo json_encode(array('var'=>$var));
	}

	public function getFriendList()
	{
		$json = file_get_contents('php://input');
		$data = json_decode($json,TRUE);
		if(!empty($data['UserID']))
		{
			$var = $this->admin_edit_model->getFriendsDetails($data['UserID']);
		}
		else
		{
			$var = 2;
		}
		echo json_encode(array('var'=>$var));
	}

	public function inviteFriends()
	{
		$json = file_get_contents('php://input');
		$data = json_decode($json,TRUE);
		if(!empty($data['UserID']))
		{
			$details = $this->admin_edit_model->getUserDetails($data['UserID']);
			if($details==0)
			{
				$var = 3;
			}
			else
			{
				foreach ($data['friends'] as $key) 
				{
					$this->admin_edit_model->insertInvite($key['email'],$data['UserID']);
					$this->sendMsgEmail($key['email'],$details['unique_code']);
				}
			}
		}
		else
		{
			$var = 2;
		}

		echo json_encode(array('var'=>$var));
	}

	public function sendRequest()
	{
		
	}

	public function getTaskDetails()
	{
		
	}

	public function getKeywords()
	{
		$json = file_get_contents('php://input');
		$data = json_decode($json,TRUE);
		if(!empty($data['UserID']))
		{
			$this->load->model('admin_edit_model_sec');
			$var = $this->admin_edit_model_sec->getTaskKeywords($data['taskId']);
		}
		else
		{
			$var = 2;
		}
		echo json_encode(array('var'=>$var));
	}

	public function getAnswers()
	{
		$json = file_get_contents('php://input');
		$data = json_decode($json,TRUE);
		if(!empty($data['UserID']))
		{
			$this->load->model('admin_edit_model_sec');
			$var = $this->admin_edit_model_sec->getAnswersTask($data['TaskID']);
			if($var != 0)
			{
				$i=0;
				foreach ($var as $key) 
				{
					$new[$i] = $key;
					$new[$i]['UserID'] = $data['UserID'];
					$new[$i]['TaskUserID'] = $data['TaskUserID'];
					$i++;
				}
				$var = $new;
			}
		}
		else
		{
			$var = 2;
		}
		echo json_encode(array('var'=>$var));
	}

	public function getComments()
	{
		$json = file_get_contents('php://input');
		$data = json_decode($json,TRUE);
		if(!empty($data['UserID']))
		{
			$this->load->model('admin_edit_model_sec');
			$var = $this->admin_edit_model_sec->getCommentsTask($data['TaskID']);
			if($var != 0)
			{
				$i=0;
				foreach ($var as $key) 
				{
					$new[$i] = $key;
					$new[$i]['UserID'] = $data['UserID'];
					$i++;
				}
				$var = $new;
			}
		}
		else
		{
			$var = 2;
		}
		echo json_encode(array('var'=>$var));
	}

	public function insertAnswer()
	{
		$json = file_get_contents('php://input');
		$data = json_decode($json,TRUE);
		if(!empty($data['UserID']))
		{
			$var = $this->admin_edit_model->insertAnswerModel($data['UserID'],$data['TaskID'],$data['Answer']);
		}
		else
		{
			$var = 2;
		}

		echo json_encode(array('var'=>$var));
	}

	public function insertComment()
	{
		$json = file_get_contents('php://input');
		$data = json_decode($json,TRUE);
		if(!empty($data['UserID']))
		{
			$this->load->model('admin_edit_model_sec');
			$var = $this->admin_edit_model_sec->insertCommentModel($data['UserID'],$data['TaskID'],$data['Comment'],$data['TaskUserID']);
		}
		else
		{
			$var = 2;
		}

		echo json_encode(array('var'=>$var));
	}

	public function getUserCompleteDetails()
	{
		$json = file_get_contents('php://input');
		$data = json_decode($json,TRUE);
		if(!empty($data['SearchID']))
		{
			$this->load->model('admin_edit_model_sec');
			$var = $this->admin_edit_model_sec->getUserCompleteDetailsById($data['UserID'],$data['SearchID']);
		}
		else
		{
			$var = 2;
		}

		echo json_encode(array('var'=>$var));
	}

	public function addFriend()
	{
		$json = file_get_contents('php://input');
		$data = json_decode($json,TRUE);
		if(!empty($data['UserID']))
		{
			$this->load->model('admin_edit_model_sec');
			$var = $this->admin_edit_model_sec->setUserFriend($data['UserID'],$data['SearchID']);
		}
		else
		{
			$var = 2;
		}
		echo (json_encode($var));
	}

	public function searchUser()
    {
    	$json = file_get_contents('php://input');
		$data = json_decode($json,TRUE);
		$rolls = explode(",", $data['search']);
        $search_query = array();
        foreach ($rolls as $key => $roll) {
            $roll = trim($roll);
            $details = array();
            if(strstr($roll, ':')){
                $details = explode(" ", $roll);
            }
            else{
                $details[] = $roll;
            }
            $field = array();
            foreach ($details as $key => $detail) { 
                if(strpos($detail, ':') === FALSE)
                {
                    if(ctype_digit($detail))
                    {  
                      $field_name = "mobile"; 
                    }
                    else
                    {
                        if(strstr($detail, '@'))
                        {
                            $field_name = "email";
                        }
                        else
                        {
                            $field_name = "name";
                        }
                    }
                    $field_value = $detail;
                }
                else
                {
                    $field_name = substr($detail, 0, strpos($detail, ':')); 
                    $field_value = substr($detail, strpos($detail, ':')+1);
                }
                $field[$field_name] =  $field_value;
            }
            array_push($search_query, $field);
        }
        $students_data = $this->admin_edit_model->get_details_advance($search_query, '6');
        echo (json_encode($students_data));
    }

	

	function _render_page($view, $data=null, $render=false)
	{

			$view_html = array(
			$this->load->view('base/header', $data, $render),
			$this->load->view('admin_edit/menu/header', $data, $render),
			$this->load->view($view, $data, $render),
			$this->load->view('admin_edit/menu/footer', $data, $render),
			$this->load->view('base/footer', $data, $render)
			);
		if (!$render) return $view_html;

	}
}
