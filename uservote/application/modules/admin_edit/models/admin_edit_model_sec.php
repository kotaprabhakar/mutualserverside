<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_edit_model_sec extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		$this->db_main = $this->load->database('default' , TRUE);
	}

	public function getTaskKeywords($taskId)
	{
		$query = $this->db_main->select()
		                       ->from('key_words')
		                       ->where(array("task_id"=>$taskId))
		                       ->get();
		if($query->num_rows() > 0)
		{
			return $query->result('array');
		}
		else
		{
			return 2;
		}
	}

	public function loginCheck($email,$password)
	{
	   $query = $this->db_main->select('id')
	                          ->from('users')
	                          ->where(array('email'=>$email,'password'=>$password))
	                          ->get();
	    if($query->num_rows() > 0)
		{
			$temp =  $query->result('array');
			return $temp[0]['id'];
		}
		else
		{
			return 0;
		}
	}

	public function setUserFriend($userId,$searchId)
	{
	   $new['reference_id'] = $userId;
	   $new['user_id'] = $searchId;
	   $this->db_main->insert('user_links',$new);
	   return 1;
	}

	public function isFriend($userId,$searchId)
	{
		$query = $this->db_main->select()
		                       ->from('user_links')
		                       ->where(array('reference_id'=>$userId,'user_id'=>$searchId))
		                       ->get();
		if($query->num_rows()>0)
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}

	public function getUserCompleteDetailsById($userId,$searchId)
	{
		$query = $this->db_main->select('first_name,last_name,email,phone')
		                       ->from('users')
		                       ->where(array('id'=>$searchId))
		                       ->get();
		if($query->num_rows()>0)
		{
			$details = array();
			$details['userDetails'] = $query->result('array');
			$details['friendBit'] = $this->isFriend($userId,$searchId);
			$details['career'] = $this->getUserCareerDetails($searchId);
			$details['educations'] = $this->getUserEducationsDetails($searchId);
			$details['interests'] = $this->getUserInterestsDetails($searchId);
			$details['places'] = $this->getUserPlacesDetails($searchId);
			return $details;
		}
		else
		{
			return 0;
		}

	}

	public function getUserCareerDetails($searchId)
	{
		$query = $this->db_main->select('name,designation,domain')
		                       ->from('career')
		                       ->where(array('user_id'=>$searchId))
		                       ->get();
		if($query->num_rows()>0)
		{
			return $query->result('array');
		}
		else
		{
			return 0;
		}
	}	


	public function getUserEducationsDetails($searchId)
	{
		$query = $this->db_main->select('field,institution')
		                       ->from('educations')
		                       ->where(array('user_id'=>$searchId))
		                       ->get();
		if($query->num_rows()>0)
		{
			return $query->result('array');
		}
		else
		{
			return 0;
		}
	}

	public function getUserInterestsDetails($searchId)
	{
		$query = $this->db_main->select('interest')
		                       ->from('interests')
		                       ->where(array('user_id'=>$searchId))
		                       ->get();
		if($query->num_rows()>0)
		{
			return $query->result('array');
		}
		else
		{
			return 0;
		}
	}

	public function getUserPlacesDetails($searchId)
	{
		$query = $this->db_main->select('home,place1,place2,place3')
		                       ->from('places')
		                       ->where(array('user_id'=>$searchId))
		                       ->get();
		if($query->num_rows()>0)
		{
			return $query->result('array');
		}
		else
		{
			return 0;
		}
	}

	public function insertCommentModel($userId,$taskId,$comment,$taskUserId)
	{
	    $this->db_main->insert('comments',array('comment'=>$comment,
	    	                         'task_user_id'=>$taskUserId,
	    	                         'comment_user_id'=>$userId,
	    	                         'task_id'=>$taskId));
	    if($this->db_main->affected_rows() > 0)
	    {
	    	return 1;
	    }
	    else
	    	return 0;
	}

	public function getAnswersTask($taskId)
	{
		$query = $this->db_main->select('task_answer as Answer,user_id as AnswerUserID,first_name as AnswerUsername,shared_performance.timestamp')
		                       ->from('shared_performance')
		                       ->join('users','shared_performance.user_id = users.id')
		                       ->order_by('shared_performance.timestamp','asc')
		                       ->where(array('task_id'=>$taskId))
		                       ->get();
		if($query->num_rows()>0)
		{
			return $query->result('array');
		}
		else
		{
			return 0;
		}
	}

	public function getCommentsTask()
	{
		$query = $this->db_main->select('comment as Comment,task_user_id as TaskUserID,first_name as CommentUsername,comment_user_id as CommentUserID')
		                       ->from('comments')
		                       ->join('users','comments.comment_user_id = users.id')
		                       ->where(array('task_id'=>$taskId))
		                       ->get();
		if($query->num_rows()>0)
		{
			return $query->result('array');
		}
		else
		{
			return 0;
		}
	}
}