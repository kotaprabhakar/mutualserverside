<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_edit_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		$this->db_main = $this->load->database('default' , TRUE);
	}


	public function insertGeneralDetails($data)
	{
		$this->db_main->insert('users',$data);
		if($this->db_main->affected_rows() > 0)
		{
			return $this->db_main->insert_id();
		}
		else
		{
			return false;
		}
	}

	public function insertPlacesDetails($data)
	{
		$this->db_main->insert('places',$data);
		if($this->db_main->affected_rows() > 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public function insertEducationDetails($data)
	{
		$this->db_main->insert('educations',$data);
		if($this->db_main->affected_rows() > 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public function checkEmail($emailId)
	{
		
	}

	public function insertCarrerDetails($data)
	{
		$this->db_main->insert('career',$data);
		if($this->db_main->affected_rows() > 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	

	public function insertInterestDetails($data)
	{
		$this->db_main->insert('interests',$data);
		if($this->db_main->affected_rows() > 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}


	public function getUserDetails($UserID)
	{
		$query = $this->db_main->select()
		              ->from('users')
		              ->where(array('id'=>$UserID))
		              ->get();
		if($query->num_rows()>0)
		{
			return $query->result('array');
		}
		else
		{
			return 0;
		}
	}

	public function insertInvite($email,$UserID)
	{
		$g['user_id'] = $UserID;
		$g['email'] = $email;
		$this->db_main->insert('invites',$g);
	}

	public function insertTaskDetails($task,$userId)
	{
		$new['user_id'] = $userId;
		$new['task_description'] = $task;
		$this->db_main->insert('tasks',$new);
		if($this->db_main->affected_rows() > 0)
		{
			return $this->db_main->insert_id();
		}
		else
		{
			return false;
		}

	}

	public function insertAssignes($data,$taskId,$base)
	{
		foreach ($data as $key) 
		{
		  $new['user_id'] = $key;
		  $new['task_id'] = $taskId;
		  $new['base_user'] = $base;
		  $this->db_main->insert('shared_performance',$new);
		}
		if($this->db_main->affected_rows() > 0)
		{
			return 1;
		}
		else
		{
			return 2;
		}
	}

	public function getTaskDetails($userId)
	{
		$query = $this->db_main->select()
		                       ->from('tasks')
		                       ->where(array('user_id'=>$userId))
		                       ->order_by("timestamp","asc")
		                       ->get();
		if($query->num_rows()>0)
		{
			return $query->result('array');
		}
		else
		{
			return 0;
		}

	}

	public function getSharedTaskDetails($userId)
	{
		$query = $this->db_main->select('task_id,task_description,private,shared_performance.timestamp')
		                       ->from('shared_performance')
		                       ->join('tasks','tasks.id = shared_performance.task_id')
		                       ->where(array('base_user'=>$userId))
		                       ->group_by('shared_performance.task_id')
		                       ->order_by('shared_performance.timestamp','asc')
		                       ->get();
		if($query->num_rows()>0)
		{
			return $query->result('array');
		}
		else
		{
			return 0;
		}

	}

	public function get_details_advance($search_query, $result_count = '5', $order_by_col = 'first_name')
	{

		$this->db_main->select('id,first_name,last_name ,email,phone')
		              ->from("users as data");
		foreach ($search_query as $key => $query) {
			$query_string = "";
			if(is_array($query) && array_key_exists("name", $query))
			{
				if(strlen($query['name']) === 1)
				{
				$query['name'] = $this->db_main->escape($query['name']."%");
				}
				else
				{
				$query['name'] = $this->db_main->escape("%".$query['name']."%");
				}
				$query_string .= "((( "."first_name"." LIKE ".$query['name'];	
				$query_string .= ") OR ( "."last_name"." LIKE ".$query['name'];
				$query_string .= ") OR ( "."email"." LIKE ".$query['name'].'))';
			}
			if(is_array($query) && count($query) > 0) 
			{
				if(array_key_exists("name", $query))
				{
					unset($query['name']);
					if(count($query) > 0)
					{
						$query_string .= ' AND ';
					}
				}
				else
				{
					$query_string = "";
					$query_string .= "(";
				}
				$count = 0;
				foreach ($query as $key => $value)
				{
					$count++;
					if($value !== "")
					{
						$value= $this->db_main->escape($value);
						$query_string .= $key.' = '.$value;
					}
					if($count < count($query))
					{
						$query_string .= ' AND ';
					}
				}

				$query_string .= ") ";
			}
			$this->db_main->or_where($query_string);
		}	
		
		$query = $this->db_main->limit($result_count)->order_by($order_by_col)->get();
		if($query->num_rows()>0)
			return $query->result();
		else
			return FALSE;
	}

	public function insertKeywords($data,$taskId)
	{
		foreach ($data as $key) 
		{
		  $new['keyword'] = $key;
		  $new['task_id'] = $taskId;
		  $this->db_main->insert('key_words',$new);
		}
		if($this->db_main->affected_rows() > 0)
		{
			return 1;
		}
		else
		{
			return 2;
		}
	}

	public function getAllAssignedTask($userId)
	{
		$query = $this->db_main->select('task_id,shared_performance.task_answer,task_description,first_name,last_name,email,phone,shared_performance.timestamp')
		                       ->from('shared_performance')
		                       ->join('tasks','shared_performance.task_id = tasks.id')
		                       ->join('users','users.id = tasks.user_id')
		                       ->where(array('shared_performance.user_id'=>$userId))
		                       ->order_by('shared_performance.timestamp','asc')
		                       ->get();
		if($query->num_rows()>0)
		{
			return $query->result('array');
		}
		else
		{
			return 0;
		}
	}

	public function insertAnswerModel($userId,$taskId,$answer)
	{
	    $this->db_main->insert('shared_performance',array('answer'=>$answer));
	    $this->db_main->where(array('user_id'=>$userId,'task_id'=>$taskId));
	    if($this->db_main->affected_rows() > 0)
	    {
	    	return 1;
	    }
	    else
	    	return 0;
	}
	
	public function getFriendsDetails($id)
    {
        $query = $this->db_main->select('user_id as frndId,first_name,last_name,email,timestamp')
                               ->from('user_links')
                               ->join('users','users.id = user_links.user_id')
                               ->where(array('reference_id'=>$id))
                               ->get();
        if($query->num_rows()>0)
        {
            return $query->result('array');
        }
        else
        {
            return 0;
        }
    }

}