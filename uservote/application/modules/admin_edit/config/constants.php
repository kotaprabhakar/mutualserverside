<?php
/**
 * Created by PhpStorm.
 * User: UDBHAV
 * Date: 1/22/2015
 * Time: 12:52 PM
 */

define( "ERROR_CODE",                                0 );
define( "SUCCESS_CODE",                              1 );
define( "REGULAR",                                   0 );
define( "BACKLOG",                                   1 );
define( "MAX_CREDITS",                              18 );
define( "MAX_SEMESTER",                              8 );
define( "SESSION_ID",                                3 );
define( "LABORATORY",                                2 );
define( "STARTING_BATCH_INDEX",                      1 );
define( "STUDY",                                     1 );
define( "EXAM",                                      2 );
define( "ADMIN",                               "ADMIN" );
define( "HOD",                                   "HOD" );
define( "FACULTY_ADVISER",            "FACULTY_ADVISER");
define( "FACULTY",            "FACULTY");

//Remove Buttons Access
define( "ALLOT_COURSES_DELETION",                 FALSE );
define( "FACULTY_ADVISER_DELETION",               FALSE );
define( "BATCH_DELETION",                         FALSE );
define( "COURSES_DELETION",                       FALSE );
define( "DEPARTMENT_DELETION",                    FALSE );
define( "SPECIALIZATION_DELETION",                FALSE );
define( "REGISTRATION_DELETION",                  FALSE );

// SMS API
define('SMS_USERNAME',          'nitwarangal');
define('SMS_PASSWORD',          'NITWARANGAL');
define('SMS_FROM',                   'TMWSDC');

