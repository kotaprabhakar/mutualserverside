<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//Database
$config['database']['facultyProfile'] =  "test_faculty_profile2";
$config['database']['student'] = "studentportal";

//My sql tables

//Courses
$config['tables']['courses'] =  "courses";
$config['tables']['coursesType'] =  "courses_type";

//Course Allotment
$config['tables']['detailedStructure'] = "detailed_structure";
$config['tables']['structure'] = "structure";
$config['tables']['department'] = "department";
$config['tables']['specialization'] = "specialization";
$config['tables']['departmentSpecialization'] = 'department_specialization';
$config['tables']['detailedDepartmentSpecialization'] = 'detailed_department_specialization';
$config['tables']['session'] =  "session";
$config['tables']['structureSection'] = "structure_section";
$config['tables']['regularCourses'] = "regular_courses";
$config['tables']['courseAllottedMode'] = "course_allotted_mode";
$config['tables']['section'] = "section";

//Faculty Advisors Allotment
$config['tables']['facultyCurrentData'] =  "faculty_current_data";

//Faculty Allotment
$config['tables']['courseFacultyAllotted'] = "course_faculty_allotted";
$config['tables']['registrationAllottedCourses'] = 'registration_allotted_courses';

//Registration
$config['tables']['detailedRegistration'] = 'detailed_registration';
$config['tables']['registered'] = 'registered';
$config['tables']['registeredCourses'] = 'registered_courses';

$config['tables']['faculty_designation'] =  "faculty_designation";
$config['tables']['user_table'] = "users";
$config['tables']['student'] = "student";
$config['tables']['student_data']='student_data';

//Open Elective table
 $config['tables']['elective_form'] = "elective_form";
 $config['tables']['publish'] = "publish";
 $config['tables']['classes_code'] = "classes_code";
 $config['tables']['student_electives'] = "student_electives";
 $config['tables']['student_elective_choices'] = "student_elective_choices";
 $config['tables']['open_courses'] = "courses";

/* End of file profile_config.php */
/* Location: ./application/config/profile_config.php */
