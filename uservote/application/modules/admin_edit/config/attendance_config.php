<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');



 //Attendence 
 $config['tables']['session'] = "session";
 $config['tables']['reg_allotted_courses']="registration_allotted_courses";
 $config['tables']['detailed_structure'] = "detailed_structure";
 $config['tables']['attendance_dates'] ="attendance_dates";
 $config['tables']['attendance_record'] = "attendance_record";
 $config['tables']['detailed_registration'] = "detailed_registration";
 $config['tables']['student_data'] = "student_data";

/* End of file attendance_config.php */
/* Location: ./application/config/attendance_config.php */
