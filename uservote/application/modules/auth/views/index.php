<legend><?php echo lang('index_heading');?></legend>
<p><?php echo lang('index_subheading');?></p>
<?php if(!empty($message)): ?>
	<div id="infoMessage" class="alert alert-danger"><?php echo $message;?></div>
<?php endif; ?>
<table class="table table-hover table-condensed table-striped" id="example">
	<thead>
		<tr>
		<th>User ID</th>
			<th><?php echo lang('index_fname_th');?></th>
			<th style="width:100px;"><?php echo lang('index_email_th');?></th>
			<th>Phone</th>
			<!-- <th>Department</th> -->
			<!-- <th>Designation</th> -->
			<th><?php echo lang('index_groups_th');?></th>
			<th><?php echo lang('index_status_th');?></th>
			<th><?php echo lang('index_action_th');?></th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($users as $user):?>
			<tr>
			<td><?php echo $user->id ?></td>
				<td><?php echo $user->first_name;?></td>
				<td><?php echo $user->email;?></td>
				<td><?php echo $user->phone ?></td>
				<!-- <td><?php //echo $user->department ?></td> -->
				<!-- <td><?php //echo $user->designation ?></td> -->
				<td>
					<?php foreach ($user->groups as $group):?>
						<?php echo anchor("auth/edit_group/".$group->id, $group->name) ;?><br />
					<?php endforeach?>
				</td>
				<td><?php echo ($user->active) ? anchor("auth/deactivate/".$user->id, lang('index_active_link')) : anchor("auth/activate/". $user->id, lang('index_inactive_link'));?></td>
				<td><?php echo anchor("auth/edit_user/".$user->id, 'Edit') ;?></td>
			</tr>
		<?php endforeach;?>
	</tbody>
</table>
<hr>
<p class="hidden-print"><?php echo anchor('auth/create_group', lang('index_create_group_link'))?></p>
