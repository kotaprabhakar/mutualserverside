<div class="row">
      <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 col-md-offset-2 col-lg-offset-2 col-sm-offset-2 col-xs-offset-2">

            <h1><?php echo lang('change_password_heading');?></h1>
            <?php if(!empty($message)): ?>
                  <div id="infoMessage" class="alert alert-info"><a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a><?php echo $message;?></div>
            <?php endif; ?>

            <form class="form form-horizontal" action="<?php echo base_url('auth/change_password'); ?>" accept-charset="utf-8" method="POST" role="form">
                  <br>
                  <div class="form-group">
                        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                              <?php echo lang('change_password_old_password_label', 'old_password');?> 
                        </div>
                        <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">

                              <?php echo form_input($old_password);?> 
                        </div> 
                  </div>
                  <div class="form-group">
                        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                        <label> <?php echo sprintf(lang('change_password_new_password_label'), $min_password_length);?></label>
                   </div>
                   <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                    <?php echo form_input($new_password);?>  
              </div> 
        </div>
        <div class="form-group">
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
             <?php echo lang('change_password_new_password_confirm_label', 'new_password_confirm');?>
       </div>
       <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
           <?php echo form_input($new_password_confirm);?>
     </div>
</div>
<div class="col-md-offset-4 col-xs-8 col-sm-8 col-md-8 col-lg-8">
     <button type="submit" name="submit" class="btn btn-primary btn-lg"><?php echo lang('change_password_submit_btn') ?></button>
</div>

</form>

</div>
</div>

