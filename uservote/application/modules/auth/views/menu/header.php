<!DOCTYPE html>
<html lang="en">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="Authentication of credentials">
<meta name="author" content="CRM">
<title>Login | CRM </title>

<head>
    <link href="<?php echo asset_url(); ?>css/0.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo asset_url(); ?>css/signin.css" rel="stylesheet">
</head>

<body id="body-auth">
    <div class="container">
        <div class="row" id="main_name">
            <div class="col-md-4 col-md-offset-4">
            <a href="">
                <img src="<?php  echo base_url('assets/images/byju_logo.png'); ?> " alt="CRM logo" width="100px">
            </a>
        </div>
    </div>
        <h1 class="text-info text-center"><b>CRM Portal</b></h1>
        <br>
    <div class="row">
