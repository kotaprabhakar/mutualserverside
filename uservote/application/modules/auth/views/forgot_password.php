<div class="col-md-4 col-md-offset-4">

	<?php if($message) :?>
		<div class="alert alert-danger" id="infoMessage"><a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a><?php echo $message;?></div>
	<?php endif; ?>

	<p><?php echo sprintf(lang('forgot_password_subheading'), $identity_label);?></p>
  	<div class="login-panel panel panel-default">
    	<div class="panel-heading">
      		<h3 class="panel-title">Forgot Password</h3>
    	</div>
    	<div class="panel-body">
			<?php echo form_open("auth/forgot_password");?>
      		<div class="form-group">
      			<?php echo form_input($email); ?> 
      		</div>
			<div class='form-group'>
				 <?php echo form_submit($submit);?>
      			<a href="<?php echo base_url("auth") ?>" type="button" class="btn btn-default">Go back</a>
      		</div>
			<?php echo form_close();?>
		</div>
  	</div>
</div>